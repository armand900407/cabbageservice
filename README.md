# CabbageService


![N|Solid](https://www.linuxadictos.com/wp-content/uploads/oracle-java-logo.jpg)

 ## By Engr. Armando Perea Sanchez <terry900407@gmail.com>

 ## CabbageService es un programa desarrollado en Java 8 con el objetivo de facilitar la ejecuci�n de scripts.
 
  
# Requerimientos del sistema
  
  - El sistema esta adaptado para funcionar en las plataformas de Windows 7 y Windows 10. Esta es la primera liberaci�nn para windows.
  - Se requiere tener instalado Perl para poder ejecutar los scripts en el entorno de comandos.
    Para asegurar esta funci�n este, puede consultarlo abriendo una terminal de cmd en windows y colocar:
    
       perl -version
       
   si recibe esta respuesta:
   
   This is perl 5, version 26, subversion 1 (v5.26.1) built for x86_64-linux-gnu-thread-multi
   (with 67 registered patches, see perl -V for more detail)
   
   Es claro que perl esta instalado, de lo contrario es necesario instalarlo para que todo funcione correctamente al ejecutar los scripts, 
   ya que el sistema trabaja con la terminal de cmd al ejecutar los scripts perl.
   
   - Se requiere que los scritps perls asi como los archivos CSV's esten localizados en una estructura de carpetas de la siguiente forma:
   
  Para el caso de los archivos pl
 
  - CAB5.0\src\bin\FC7.pl
  - CAB5.0\src\bin\BC5.pl
  
  Para el caso de los archivos csv's
  
  - CAB5.0\DatosParaBC4\trainingfile.csv
  - CAB5.0\DatosParaBC4\metadata.csv
  - CAB5.0\DatosParaFC7\trainingfile.csv
  - CAB5.0\DatosParaFC7\metadata.csv
  
  Con esto aseguramos que el proceso de automatizaci�n del sistema ayude al usuario a detectar mas rapidamente los csvs y se 
  autocompleten al momento de elegir el script.
  
  - Se requiere tener instalado  el JDK8.0 que puede descargarse de su pagina oficial para 32 y 64 bits:
  
    https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
    
   descargue el zip, descomprimalo y ejecutelo, esto instalar� todas las dependencias para Java asi como su maquina virtual para que
   los programas en java puedan ejecutarse sin problema.S

# Automatizaci�n de selecci�n de scripts y csv'ss

   ## El proceso de automatizaci�n se centra apartir de la selecci�n del script perl a ejecutar (FC7 o BC4)
   
  - El primer paso es seleccionar una de las 2 opciones presentes en las pesta�as, sean estas FC7 o BC4.

  - Posteriormente al seleccionar el script perl, poniendo como ejemplo (CAB5.0\src\bin\FC7.pl),el sistema internamente
  	tomar� como referencia la raiz CAB5.0 y apartir de ahi, construir� una ruta absoluta apuntando 
  	a los archivos correspondientes de trainingfile.csv y metadatafile.csv y los colocar� en autom�tico en los campos
  	correspondientes a csv1 y csv2 asi como el OUTPATH se generar� por default en la carpeta CAB5.0\ que es donde 
  	se generar�n los resultados de la ejecuci�n del script.
  	
  - Con este proceso interno del sistema, es suficiente con seleccionar el perl script, y llenar en un proceso automatizado
  	los campos de los csv's especificados por default y el usuario solo debe seleccionar el tipo de operaci�n ("X2", "IG", "OR", 	"LO", "MI") que estan desplegados en el combo,
  	esto en el caso de FC7, para BC4 no hay operaciones a elegir ya que el proceso solo ocupa 3 csv sin parametros de entrada. Posteriormente ejecutar el script con el boton de "Execute Script", 
  	en ese momento se ejecutar� la terminal de comandos de windows ya con el comando de ejecuci�n trabajando, solo para que el usuario
  	seleccione el contexto de la clase que requiere generar cuando se abra la pantalla de cmd de windows.
  	
  - Una vez generado el archivo en el OutPath, y corroborar que todo se ejecut� correctamente, puede volver a utilizar el
  	servicio de ejecuci�n de scripts de cabbage, al momento notar� que aun hay valores en los campos de la última ejecuci�n
  	realizada, simplemente presione el bot�n de "Clear Fields" y esto habilitara nuevamente todo el servicio para volver
  	a iniciar un proceso de selecci�n de scripts.


# Proceso manual de selecci�n de scripts y csv'ss

  - Este proceso se activa cuando al seleccionar el script de perl, el sistema detecta que no esta 
    dentro de la ruta espcificada "CAB5.0\src\bin\FC7.pl", es decir, el script no esta dentro de una estructura v�lida
    y enviar� un mensaje advirtiendo que es posible que el script no funcione correctamente y en este caso
    no se colocar�n las rutas para los archivos csv's ya que se esta realizando un flujo ajeno al especificado, 
    por lo que se tendr� que seleccionar manualmente la ruta para ambos csv's, y por default el OutPath solo quedar�
    en la ruta de C:\\ ya que es un flujo ajeno al esperado, por lo que si el script llegase a funcionar, se generar�
    en C:\\.


# Es posible evitar el proceso de automatizaci�n al seleccionar el script a ejecutar?

   - El proceso de autollenado de los campos es una funci�n que por comodidad del usuario siempre ocurrir� en autom�tico
     al seleccionar el script, sin embargo, el usuario es libre de seleccionar manualmente los csv's con el boton de 
     "Search CSV" al hacer este proceso, se sobreescribir� la ruta del archivo en el campo, y el campo posterior se reiniciara para asegurar la integridad de los
     inputs.
     
# Hay posibilidad de agregar mas processos aparte de FC7 y BC4 ?

- Por supuesto que si, el proyecto en java debe seguir la modularidad de los principios SOLID, es decir, deben estar cerrados
  a modificaciones y abiertos a extensiones, por lo que el proceso seria agregar una pestal�a por proceso lo cual es relativamente sencillo.

# M�s informaci�n sobre el proyecto y atenci�n a issues:

- Ing. Armando Perea
  
- Email : terry900407@gmail.com
	
- Cualquier duda o comentario con gusto quedo a sus ordenes.
	
- Adjunto repositorio del proyecto para que si desean mejorarlo sean libres de hacerlo.
	
 - GIT REPOSITORY: 
 
 https://gitlab.com/armand900407/cabbageservice.git
 



