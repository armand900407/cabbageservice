package com.cabbage.terminal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ProcessBuilderTerminal {
	
	
	public static void main(String[] args) throws IOException, InterruptedException {
		
		List<String> cmds = new ArrayList<String>();
		cmds.add("perl");
		cmds.add("/home/armando/Documents/CAB5.0/src/bin/FC7.pl");
		cmds.add("/home/armando/Documents/CAB5.0/DatosParaFC7/metadatafile.csv");
		cmds.add("/home/armando/Documents/CAB5.0/DatosParaFC7/trainingfile.csv");
		cmds.add("outpath");
		cmds.add("X2");

		ProcessBuilder processBuilder = new ProcessBuilder(cmds);
		System.out.println("Point: "+processBuilder);
		Process process = processBuilder.start();
		System.out.println("Point: "+process);
		  /* dump output stream */
        BufferedReader is = new BufferedReader( new InputStreamReader(process.getInputStream()));
        String sLine;
        System.out.println("Point: "+is.readLine());
        while ((sLine = is.readLine()) != null) {
            System.out.println(sLine);
            
        }
        process.waitFor();
		is.close();
		System.out.println("ok!");

		System.exit(0);
		
	}

}
