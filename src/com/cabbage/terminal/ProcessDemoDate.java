package com.cabbage.terminal;


import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ProcessDemoDate {

   public static void main(String[] args) {

      System.out.println
         ("*************Calendar for Year**********");
      try {
         ProcessBuilder pb = new
            ProcessBuilder("cal", "2022");
         final Process p=pb.start();
         BufferedReader br=new BufferedReader(
            new InputStreamReader(
               p.getInputStream()));
            String line;
            while((line=br.readLine())!=null){
               System.out.println(line);
            }
      } catch (Exception ex) {
         System.out.println(ex);
      }
      System.out.println
         ("************************************");
   }
}