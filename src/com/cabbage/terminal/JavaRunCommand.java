package com.cabbage.terminal;

import java.io.*;

public class JavaRunCommand {

    public static void main(String args[]) throws IOException {

    	Runtime rt = Runtime.getRuntime();
    	
    	 // LINUX //
        String[] aCmdArgs = { "perl","/home/armando/Documents/CAB5.0/src/bin/FC7.pl",
                "/home/armando/Documents/CAB5.0/DatosParaFC7/metadatafile.csv",
                "/home/armando/Documents/CAB5.0/DatosParaFC7/trainingfile.csv",
                "Output",
                "X2"};
        
        Runtime runtime = Runtime.getRuntime();
        Process process = runtime.exec(aCmdArgs);

        BufferedReader lineReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        lineReader.lines().forEach(System.out::println);

        BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        errorReader.lines().forEach(System.out::println);


	}
    
}
