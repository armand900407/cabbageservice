package com.cabbage.terminal;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

/**
 * @author Armando Perea &lt;APerea@express-scripts.com&gt;
 * @version 1.0.0
 * @since 1.0.0
 */
public class ExecutePerl {
    
    public static void main(String[] args) throws IOException {
    	// WINDOWS //
//        String[] aCmdArgs = { "C:\\Perl64\\bin\\perl.exe","C:\\Users\\ArmandoPereaSanchez\\Documents\\Perl\\CAB5.0\\src\\bin\\FC7.pl",
//                "C:\\Users\\ArmandoPereaSanchez\\Documents\\Perl\\CAB5.0\\DatosParaFC7\\metadatafile.csv",
//                "C:\\Users\\ArmandoPereaSanchez\\Documents\\Perl\\CAB5.0\\DatosParaFC7\\trainingfile.csv",
//                "outpath",
//                "X2" };
        
        // LINUX //
        String[] aCmdArgs = { "perl","/home/armando/Documents/CAB5.0/src/bin/FC7.pl",
                "/home/armando/Documents/CAB5.0/DatosParaFC7/metadatafile.csv",
                "/home/armando/Documents/CAB5.0/DatosParaFC7/trainingfile.csv",
                "Output",
                "X2"};
        
        Runtime oRuntime = Runtime.getRuntime();
        Process oProcess = null;
        
        try {
            oProcess = oRuntime.exec(aCmdArgs);
            PrintWriter writer = new PrintWriter(new OutputStreamWriter(
                    oProcess.getOutputStream()));
            writer.println(200);
            writer.close();
            oProcess.waitFor();
            
        } catch (Exception e) {
            System.out.println("error executing " + aCmdArgs[0]);
        }
        
        /* dump output stream */
        BufferedReader is = new BufferedReader
            ( new InputStreamReader(oProcess.getInputStream()));
        String sLine;
        while ((sLine = is.readLine()) != null) {
            System.out.println(sLine);
            
        }
       
        
//       //Enter data using BufferReader 
//        BufferedReader reader =  
//                   new BufferedReader(new InputStreamReader(System.in)); 
//         
//        // Reading data using readLine 
//        String name = reader.readLine(); 
//  
//        // Printing the read line 
//        System.out.flush();
//        
//        /* print final result of process */
        //System.err.println("Exit status=" + oProcess.exitValue());
        
        return;
    }

}
