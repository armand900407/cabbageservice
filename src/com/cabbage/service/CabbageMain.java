/*
 *      File: CabbageMain.java
 *    Author: Armando Perea <APerea@express-scripts.com>
 *      Date: Apr 08, 2019
 * Copyright: Express Scripts 2019
 */

/**
 * 
 */
package com.cabbage.service;

import java.io.IOException;

/**
 * @author Armando Perea &lt;APerea@express-scripts.com&gt;
 * @version 1.0.0
 * @since 1.0.0
 */
public class CabbageMain {
    
    public static void main(String[] args)  {
        new CabbageExecuterService().setVisible(true);
    }

}
