package com.cabbage.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileSystemView;

public class JavaPerlService {
	
	public static final String SCRIPT_NOT_FOUND="SCRIPT FILE NOT FOUND!";
	
	public static final String SUCCESS_SCRIPT="SCRIPT SUCCESSFULLY RUN!";
	
	
	
	
	public static String ScriptPathFilterFC7(String operation, String type) {
		String commandExe = operation+" "+type;
		String response=SUCCESS_SCRIPT;
		if(operation != null) {
			response = runPerlScriptFC7(commandExe);
		}
		return response;
	}
	
	public static String runPerlScriptFC7(String command) {
	    String response = SUCCESS_SCRIPT;
	    System.out.println("Executing.... "+command);
	    try {
            Runtime.getRuntime().exec("cmd /c start cmd.exe /K \""+"perl "+command+"");
        } catch (IOException e) {
            response=SCRIPT_NOT_FOUND;
            e.printStackTrace();
        } 
	    return response;
	}
	
	public static String ScriptPathFilterBC4(String operation) {
		String commandExe = operation;
		String response=SUCCESS_SCRIPT;
		if(operation != null) {
			response = runPerlScriptBC4(commandExe);
		}
		return response;
	}
	
	public static String runPerlScriptBC4(String command) {
	    String response = SUCCESS_SCRIPT;
        System.out.println("Executing.... "+command);
        try {
            Runtime.getRuntime().exec("cmd /c start cmd.exe /K \""+"perl "+command+"");
        } catch (IOException e) {
            response=SCRIPT_NOT_FOUND;
            e.printStackTrace();
        } 
        return response;
	}
	
	public static String ExePathVampVH5(String exeVampFile,String probeFileVamp,String targetListVamp,String outFileVamp,String gcutOffVamp,String misMatchesVamp ,String cmbStrand) {
        String commandExe = exeVampFile+" -PROBEFILE "+probeFileVamp+" -TARGETLIST "+targetListVamp+" -OUTFILE "+outFileVamp+".txt "+"-GCUTOFF "+ gcutOffVamp + " -MISMATCHES "+ misMatchesVamp+ " -STRAND "+cmbStrand;
        String response=SUCCESS_SCRIPT;
        if(exeVampFile != null) {
            response = runExeScriptVampVH5(commandExe);
        }
        return response;
    }
    
    public static String runExeScriptVampVH5(String command) {
        String response = SUCCESS_SCRIPT;
        System.out.println("Executing.... "+command);
        try {
            //Process process = new ProcessBuilder(command).start();
            Runtime.getRuntime().exec("cmd /c start cmd.exe /K \""+command);
        } catch (IOException e) {
            response=SCRIPT_NOT_FOUND;
            e.printStackTrace();
        } 
        return response;
    }
    
    public static String ExePathVampVHRP(String vhrpExeVamp,String vhDataFileVHRP,String probeFileVHRP,String targetListVHRP,String globalFileVHRP,String sitesGlobalVHRP) {
        String commandExe = vhrpExeVamp+" -VHDATAFILE "+vhDataFileVHRP+" -PROBEFILE "+probeFileVHRP+" -TARGETLIST "+targetListVHRP+" -GLOBALFILE "+ globalFileVHRP+".txt" + " -SITESGLOBAL "+ sitesGlobalVHRP;
        String response=SUCCESS_SCRIPT;
        if(vhrpExeVamp != null) {
            response = runExeScriptVampVHRP(commandExe);
        }
        return response;
    }
    
    public static String runExeScriptVampVHRP(String command) {
        String response = SUCCESS_SCRIPT;
        System.out.println("Executing.... "+command);
        try {
            //Process process = new ProcessBuilder(command).start();
            Runtime.getRuntime().exec("cmd /c start cmd.exe /K \""+command);
        } catch (IOException e) {
            response=SCRIPT_NOT_FOUND;
            e.printStackTrace();
        } 
        return response;
    }
    
    public static String ExePathVampVFAT(String vfatExeVamp,String vFatVhFile,String targetListVfat,String vfatOutFile,String vfatLeftText,String vfatRightText,String vfatThreshold,
            String vfatFormat,String vfatMode,String vfatModel,String vfatCount,String vfatTracking,String vfatTrackFile,String vfatTrackext,String vfatReplicates) {
        String commandExe = vfatExeVamp+" -VHFILE "+vFatVhFile+" -TARGETLIST "+targetListVfat+" -OUTFILE "+vfatOutFile+".txt "+"-LEFTEXT "+ vfatLeftText + " -RIGHTEXT "+ vfatRightText+ 
                " -THRESHOLD "+vfatThreshold+" -FORMAT "+vfatFormat+" -MODE "+vfatMode+" -MODEL "+vfatModel+" -COUNTS "+vfatCount+" -TRACKING "+vfatTracking+" -TRACKFILE "+vfatTrackFile+".txt "+
                " -TRACKEXT "+vfatTrackext+" -REPLICATES "+vfatReplicates;
        String response=SUCCESS_SCRIPT;
        if(vfatExeVamp != null) {
            response = runExeScriptVampVFAT(commandExe);
        }
        return response;
    }
    
    public static String runExeScriptVampVFAT(String command) {
        String response = SUCCESS_SCRIPT;
        System.out.println("Executing.... "+command);
        try {
            //Process process = new ProcessBuilder(command).start();
            Runtime.getRuntime().exec("cmd /c start cmd.exe /K \""+command);
        } catch (IOException e) {
            response=SCRIPT_NOT_FOUND;
            e.printStackTrace();
        } 
        return response;
    }
    
	public static String fileChooser() {
		JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		String path=null;
		int returnValue = jfc.showOpenDialog(null);
		// int returnValue = jfc.showSaveDialog(null);
		File selectedFile = jfc.getSelectedFile();
		if (returnValue == JFileChooser.APPROVE_OPTION) {
		    System.out.println(selectedFile.getAbsolutePath());
		    path=" "+selectedFile.getAbsolutePath(); 
		    }
			
			return path;
		}
		
}