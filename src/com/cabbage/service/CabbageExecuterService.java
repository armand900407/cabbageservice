/*
 *      File: Frame.java
 *    Author: Armando Perea <APerea@express-scripts.com>
 *      Date: Apr 03, 2019
 * Copyright: Express Scripts 2019
 */

/**
 * 
 */
package com.cabbage.service;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTabbedPane;
import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;

/**
 * @author Armando Perea &lt;APerea@express-scripts.com&gt;
 * @version 1.0.0
 * @since 1.0.0
 */
public class CabbageExecuterService extends JFrame {
    
    private JPanel contentPane;
    private JTextField txtScriptPerlFC7;
    private JTextField txtScriptOneFC7;
    private JTextField txtScriptTwoFC7;
    private JTextField txtOutPathFC7;
    private JTextField txtScriptPerlBC4;
    private JTextField txtScriptOneBC4;
    private JTextField txtScriptTwoBC4;
    private JTextField txtOutPathBC4;
    //public static JTextArea txtReceivedMessageBC4;
    //public static JTextArea txtReceivedMessageFC7;
    //public static JTextArea txtReceivedMessageVamp;
    public static String receivedMessageBC4;
    public static String receivedMessageFC7;
    public static String receivedMessageVamp;
    private JTextField txtScriptThreeBC4;
    private JTextField txtExeVampFile;
    private JTextField txtProbeFileVamp;
    private JTextField txtTargetListVamp;
    private JTextField txtGcutOffVamp;
    private JTextField txtOutFileVamp;
    private JTextField txtMisMatchesVamp;
    private JTextField txtVfatExeVamp;
    private JTextField txtVFatVhFile;
    private JTextField txtTargetListVfat;
    private JTextField txtVfatRightText;
    private JTextField txtVfatOutFile;
    private JTextField txtVfatLeftText;
    private JTextField txtVfatThreshold;
    private JTextField txtVfatTrackFile;
    private JTextField txtVfatReplicates;
    private JTextField txtVhrpExeVamp;
    private JTextField txtVhDataFileVHRP;
    private JTextField txtTargetListVHRP;
    private JTextField txtGlobalFileVHRP;
    private JTextField txtProbeFileVHRP;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    CabbageExecuterService frame = new CabbageExecuterService();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     * @throws IOException 
     */
    public CabbageExecuterService() {
    	setResizable(false);
        initialize() ; 
    }
    
       private void initialize() {
        setTitle("Cabbage/Vamp Executor Service");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 715, 692);
        contentPane = new JPanel();
        contentPane.setBorder(null);
        setContentPane(contentPane);
        contentPane.setLayout(new GridLayout(0, 1, 0, 0));
        
        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        contentPane.add(tabbedPane);
        
        JPanel panelFC7 = new JPanel();
        panelFC7.setLayout(null);
        panelFC7.setBackground(Color.WHITE);
        tabbedPane.addTab("FC7 PROCESS", null, panelFC7, null);
        
        JComboBox cmbScriptOptionFC7 = new JComboBox();
        cmbScriptOptionFC7.setModel(new DefaultComboBoxModel(new String[] {"SELECT AN OPTION ......", "X2", "IG", "OR", "LO", "MI"}));
        cmbScriptOptionFC7.setBounds(238, 373, 205, 31);
        panelFC7.add(cmbScriptOptionFC7);
        
        JLabel label_1 = new JLabel("");
        label_1.setIcon(new ImageIcon(CabbageExecuterService.class.getResource("/com/cabbage/service/images/cab2.jpg")));
        label_1.setBounds(238, 0, 245, 182);
        panelFC7.add(label_1);
        
        JLabel label_2 = new JLabel("Select a script file:");
        label_2.setForeground(new Color(0, 128, 0));
        label_2.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        label_2.setBounds(27, 202, 181, 31);
        panelFC7.add(label_2);
        
        JButton btnSearchScriptFC7 = new JButton("SCRIPT FC7");
        btnSearchScriptFC7.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                setScriptsPathsFC7("FC7");
            }
        });
        btnSearchScriptFC7.setBounds(536, 208, 121, 25);
        panelFC7.add(btnSearchScriptFC7);
        
        txtScriptPerlFC7 = new JTextField();
        txtScriptPerlFC7.setToolTipText("Choose Perl Script");
        txtScriptPerlFC7.setForeground(Color.BLACK);
        txtScriptPerlFC7.setFont(new Font("Dialog", Font.BOLD, 14));
        txtScriptPerlFC7.setEnabled(false);
        txtScriptPerlFC7.setColumns(10);
        txtScriptPerlFC7.setBackground(Color.ORANGE);
        txtScriptPerlFC7.setBounds(218, 205, 302, 27);
        panelFC7.add(txtScriptPerlFC7);
        
        txtScriptOneFC7 = new JTextField();
        txtScriptOneFC7.setToolTipText("Choose CSV 1");
        txtScriptOneFC7.setForeground(Color.BLACK);
        txtScriptOneFC7.setFont(new Font("Dialog", Font.BOLD, 14));
        txtScriptOneFC7.setEnabled(false);
        txtScriptOneFC7.setColumns(10);
        txtScriptOneFC7.setBackground(Color.ORANGE);
        txtScriptOneFC7.setBounds(218, 243, 302, 27);
        panelFC7.add(txtScriptOneFC7);
        
        txtScriptTwoFC7 = new JTextField();
        txtScriptTwoFC7.setToolTipText("Choose CSV 2");
        txtScriptTwoFC7.setForeground(Color.BLACK);
        txtScriptTwoFC7.setFont(new Font("Dialog", Font.BOLD, 14));
        txtScriptTwoFC7.setEnabled(false);
        txtScriptTwoFC7.setColumns(10);
        txtScriptTwoFC7.setBackground(Color.ORANGE);
        txtScriptTwoFC7.setBounds(218, 281, 302, 27);
        panelFC7.add(txtScriptTwoFC7);
        
        txtOutPathFC7 = new JTextField();
        txtOutPathFC7.setToolTipText("OutPath");
        txtOutPathFC7.setForeground(Color.BLACK);
        txtOutPathFC7.setFont(new Font("Dialog", Font.BOLD, 14));
        txtOutPathFC7.setEnabled(false);
        txtOutPathFC7.setColumns(10);
        txtOutPathFC7.setBackground(Color.ORANGE);
        txtOutPathFC7.setBounds(218, 319, 302, 27);
        panelFC7.add(txtOutPathFC7);
        
        JPanel panelBC4 = new JPanel();
        panelBC4.setLayout(null);
        panelBC4.setBackground(Color.WHITE);
        tabbedPane.addTab("BC4 PROCESS", null, panelBC4, null);
        
        JLabel label_7 = new JLabel("");
        label_7.setIcon(new ImageIcon(CabbageExecuterService.class.getResource("/com/cabbage/service/images/cab2.jpg")));
        label_7.setBounds(237, 0, 245, 193);
        panelBC4.add(label_7);
        
        JLabel label_8 = new JLabel("Select a script file:");
        label_8.setForeground(new Color(0, 128, 0));
        label_8.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        label_8.setBounds(27, 201, 181, 31);
        panelBC4.add(label_8);
        
        JButton btnSearchScriptBC4 = new JButton("SCRIPT BC4");
        btnSearchScriptBC4.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		setScriptsPathsBC4("BC4");
        	}
        });
        btnSearchScriptBC4.setBounds(535, 207, 121, 25);
        panelBC4.add(btnSearchScriptBC4);
        
        JButton btnExecuteScriptBC4 = new JButton("Execute Script!");
        btnExecuteScriptBC4.setBackground(Color.GREEN);
        btnExecuteScriptBC4.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
                //String operation = txtReceivedMessageBC4.getText();
                String receivedMessage;
                System.out.println("ReceivedMessageBC4: "+receivedMessageBC4);
                
                    if(txtScriptPerlBC4.getText().isEmpty() || txtScriptOneBC4.getText().isEmpty() || txtScriptTwoBC4.getText().isEmpty() || 
                    		txtScriptThreeBC4.getText().isEmpty() || txtOutPathBC4.getText().isEmpty()) {
                        JOptionPane.showMessageDialog(null,"NO SCRIPTS TO PROCESS, PLEASE SELECT SCRIPTS OR CSV'S");
                    }else {
                        receivedMessage = JavaPerlService.ScriptPathFilterBC4(receivedMessageBC4);
                        System.out.println("RECEIVED MESSAGE: "+receivedMessage);
                    }
                
        	}
        });
        btnExecuteScriptBC4.setBounds(295, 432, 154, 31);
        panelBC4.add(btnExecuteScriptBC4);
        
        JLabel lblTrainFile = new JLabel("Training File :");
        lblTrainFile.setForeground(new Color(0, 128, 0));
        lblTrainFile.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblTrainFile.setBounds(63, 243, 145, 31);
        panelBC4.add(lblTrainFile);
        
        JButton btnCsvOneBC4 = new JButton("CSV TRAIN");
        btnCsvOneBC4.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		String path = JavaPerlService.fileChooser();
                String perlScript = txtScriptPerlBC4.getText();
                String outPath = txtOutPathBC4.getText(); 
//                txtReceivedMessageBC4.setText(null);
//                txtReceivedMessageBC4.setText(perlScript+" \n "+path+" \n "+outPath);
                receivedMessageBC4 = null;
                receivedMessageBC4 = perlScript+" \n "+path+" \n "+outPath;
                txtScriptOneBC4.setText(path);
                txtScriptOneBC4.setBackground(Color.WHITE);
                txtScriptTwoBC4.setText(null);
                txtScriptTwoBC4.setBackground(Color.ORANGE);
                txtScriptThreeBC4.setText(null);
                txtScriptThreeBC4.setBackground(Color.ORANGE);
        	}
        });
        btnCsvOneBC4.setBounds(535, 243, 121, 25);
        panelBC4.add(btnCsvOneBC4);
        
        JButton btnCsvTwoBC4 = new JButton("CSV META");
        btnCsvTwoBC4.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		String path = JavaPerlService.fileChooser();
      		  	String perlScript = txtScriptPerlBC4.getText();
      		  	String script1 = txtScriptOneBC4.getText();
      		  	String outPath = txtOutPathBC4.getText(); 
//      		  	txtReceivedMessageBC4.setText(null);
//      		  	txtReceivedMessageBC4.setText(perlScript+" \n "+script1+" \n "+path+" \n "+outPath);
      		  	receivedMessageBC4 = null;
      		  	receivedMessageBC4 = perlScript+" \n "+script1+" \n "+path+" \n "+outPath;
      		  	txtScriptTwoBC4.setText(path);
      		  	txtScriptTwoBC4.setBackground(Color.WHITE);
      		  	txtScriptThreeBC4.setText(null);
      		  	txtScriptThreeBC4.setBackground(Color.ORANGE);
        	}
        });
        btnCsvTwoBC4.setBounds(535, 279, 121, 25);
        panelBC4.add(btnCsvTwoBC4);
        
        JLabel lblMetaFile = new JLabel("MetaData File :");
        lblMetaFile.setForeground(new Color(0, 128, 0));
        lblMetaFile.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblMetaFile.setBounds(52, 285, 156, 31);
        panelBC4.add(lblMetaFile);
        
        JLabel label_11 = new JLabel("OutPath:");
        label_11.setForeground(new Color(0, 128, 0));
        label_11.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        label_11.setBounds(117, 361, 91, 31);
        panelBC4.add(label_11);
        
        txtScriptPerlBC4 = new JTextField();
        txtScriptPerlBC4.setToolTipText("Choose Perl Script");
        txtScriptPerlBC4.setForeground(Color.BLACK);
        txtScriptPerlBC4.setFont(new Font("Dialog", Font.BOLD, 14));
        txtScriptPerlBC4.setEnabled(false);
        txtScriptPerlBC4.setColumns(10);
        txtScriptPerlBC4.setBackground(Color.ORANGE);
        txtScriptPerlBC4.setBounds(219, 204, 295, 27);
        panelBC4.add(txtScriptPerlBC4);
        
        txtScriptOneBC4 = new JTextField();
        txtScriptOneBC4.setToolTipText("Choose CSV 1");
        txtScriptOneBC4.setForeground(Color.BLACK);
        txtScriptOneBC4.setFont(new Font("Dialog", Font.BOLD, 14));
        txtScriptOneBC4.setEnabled(false);
        txtScriptOneBC4.setColumns(10);
        txtScriptOneBC4.setBackground(Color.ORANGE);
        txtScriptOneBC4.setBounds(218, 246, 296, 27);
        panelBC4.add(txtScriptOneBC4);
        
        txtScriptTwoBC4 = new JTextField();
        txtScriptTwoBC4.setToolTipText("Choose CSV 2");
        txtScriptTwoBC4.setForeground(Color.BLACK);
        txtScriptTwoBC4.setFont(new Font("Dialog", Font.BOLD, 14));
        txtScriptTwoBC4.setEnabled(false);
        txtScriptTwoBC4.setColumns(10);
        txtScriptTwoBC4.setBackground(Color.ORANGE);
        txtScriptTwoBC4.setBounds(218, 284, 295, 27);
        panelBC4.add(txtScriptTwoBC4);
        
        txtOutPathBC4 = new JTextField();
        txtOutPathBC4.setToolTipText("OutPath");
        txtOutPathBC4.setForeground(Color.BLACK);
        txtOutPathBC4.setFont(new Font("Dialog", Font.BOLD, 14));
        txtOutPathBC4.setEnabled(false);
        txtOutPathBC4.setColumns(10);
        txtOutPathBC4.setBackground(Color.ORANGE);
        txtOutPathBC4.setBounds(219, 364, 295, 27);
        panelBC4.add(txtOutPathBC4);
        
        JButton btnClearBC4 = new JButton("Clear Fields");
        btnClearBC4.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		txtScriptPerlBC4.setText(null);
                txtScriptPerlBC4.setBackground(Color.ORANGE);
                txtScriptOneBC4.setText(null);
                txtScriptOneBC4.setBackground(Color.ORANGE);
                txtScriptTwoBC4.setText(null);
                txtScriptTwoBC4.setBackground(Color.ORANGE);
                txtScriptThreeBC4.setText(null);
                txtScriptThreeBC4.setBackground(Color.ORANGE);
                txtOutPathBC4.setText(null);
                txtOutPathBC4.setBackground(Color.ORANGE);
                receivedMessageBC4=null;

        	}
        });
        btnClearBC4.setForeground(Color.WHITE);
        btnClearBC4.setBackground(new Color(0, 204, 204));
        btnClearBC4.setBounds(315, 495, 102, 23);
        panelBC4.add(btnClearBC4);
        
        txtScriptThreeBC4 = new JTextField();
        txtScriptThreeBC4.setToolTipText("Choose CSV Query");
        txtScriptThreeBC4.setForeground(Color.BLACK);
        txtScriptThreeBC4.setFont(new Font("Dialog", Font.BOLD, 14));
        txtScriptThreeBC4.setEnabled(false);
        txtScriptThreeBC4.setColumns(10);
        txtScriptThreeBC4.setBackground(Color.ORANGE);
        txtScriptThreeBC4.setBounds(219, 322, 295, 27);
        panelBC4.add(txtScriptThreeBC4);
        
        JLabel lblAddCsvFile = new JLabel("Query File :");
        lblAddCsvFile.setForeground(new Color(0, 128, 0));
        lblAddCsvFile.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblAddCsvFile.setBounds(87, 319, 121, 31);
        panelBC4.add(lblAddCsvFile);
        
        JButton btnCsvThreeBC4 = new JButton("CSV QUERY");
        btnCsvThreeBC4.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		String path = JavaPerlService.fileChooser();
      		  	String perlScript = txtScriptPerlBC4.getText();
      		  	String script1 = txtScriptOneBC4.getText();
      		  	String script2 = txtScriptTwoBC4.getText();
      		  	String outPath = txtOutPathBC4.getText(); 
//      		  	txtReceivedMessageBC4.setText(null);
//      		  	txtReceivedMessageBC4.setText(perlScript+" \n "+script1+" \n "+script2+" \n "+path+" \n "+outPath);
      		    receivedMessageBC4=null;
                receivedMessageBC4=perlScript+" \n "+script1+" \n "+script2+" \n "+path+" \n "+outPath;
      		  	txtScriptThreeBC4.setText(path);
      		  	txtScriptThreeBC4.setBackground(Color.WHITE);
        	}
        });
        btnCsvThreeBC4.setBounds(535, 325, 121, 25);
        panelBC4.add(btnCsvThreeBC4);
        
        JLabel lblBc = new JLabel("BC4");
        lblBc.setForeground(new Color(0, 128, 0));
        lblBc.setFont(new Font("Noto Sans CJK TC Regular", Font.BOLD | Font.ITALIC, 30));
        lblBc.setBounds(86, 12, 91, 82);
        panelBC4.add(lblBc);
        
//        URL iconURL;
//        iconURL = this.getClass().getClassLoader().getResource("/com/cabbage/service/images/cab2.jpg");
//        Image iconCorner = new ImageIcon(iconURL).getImage();
        //setIconImage(ImageIO.read(getClass().getResourceAsStream("/cab2.jpg")));
        
        JButton btnExecuteScriptFC7 = new JButton("Execute Script!");
        btnExecuteScriptFC7.setBackground(Color.GREEN);
        btnExecuteScriptFC7.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //String operation = txtReceivedMessageFC7.getText();
                System.out.println("ReceivedMessageFC7: "+receivedMessageFC7);
                String receivedMessage;
                int selectedItem = cmbScriptOptionFC7.getSelectedIndex();
                
                if(selectedItem != 0) {
                    if(txtScriptPerlFC7.getText().isEmpty() || txtScriptOneFC7.getText().isEmpty() || txtScriptTwoFC7.getText().isEmpty() || 
                        txtOutPathFC7.getText().isEmpty()) {
                        JOptionPane.showMessageDialog(null,"NO SCRIPTS TO PROCESS, PLEASE SELECT SCRIPTS OR CSV'S");
                    }else {
                        String operationType = (String) cmbScriptOptionFC7.getSelectedItem();
                        receivedMessage = JavaPerlService.ScriptPathFilterFC7(receivedMessageFC7,operationType);
                        System.out.println("RECEIVED MESSAGE: "+receivedMessage);
                    }
                }else {
                    JOptionPane.showMessageDialog(null,"PLEASE SELECT AN OPTION OPERATION");
                } 
            }
        });
        btnExecuteScriptFC7.setBounds(271, 430, 154, 31);
        panelFC7.add(btnExecuteScriptFC7);
        
        JLabel lblTrainingFile = new JLabel("Training File :");
        lblTrainingFile.setForeground(new Color(0, 128, 0));
        lblTrainingFile.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblTrainingFile.setBounds(64, 240, 144, 31);
        panelFC7.add(lblTrainingFile);
        
        JButton btnCsvOneFC7 = new JButton("CSV TRAINING");
        btnCsvOneFC7.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		String path = JavaPerlService.fileChooser();
                String perlScript = txtScriptPerlFC7.getText();
                String outPath = txtOutPathFC7.getText(); 
//                txtReceivedMessageFC7.setText(null);
//                txtReceivedMessageFC7.setText(perlScript+" \n "+path+" \n "+outPath);
                receivedMessageFC7 = null;
                receivedMessageFC7 = perlScript+" \n "+path+" \n "+outPath;
                txtScriptOneFC7.setText(path);
                txtScriptOneFC7.setBackground(Color.WHITE);
                txtScriptTwoFC7.setText(null);
                txtScriptTwoFC7.setBackground(Color.ORANGE);
                

        	}
        });
        btnCsvOneFC7.setBounds(536, 246, 121, 25);
        panelFC7.add(btnCsvOneFC7);
        
        JButton btnCsvTwoFC7 = new JButton("CSV META");
        btnCsvTwoFC7.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		String path2 = JavaPerlService.fileChooser();
                String perlScript = txtScriptPerlFC7.getText();
                String path1 = txtScriptOneFC7.getText();
                String outPath = txtOutPathFC7.getText(); 
//                txtReceivedMessageFC7.setText(null);
//                txtReceivedMessageFC7.setText(perlScript+"\n "+path1+" \n "+path2+" \n "+outPath);
                receivedMessageFC7 = null;
                receivedMessageFC7 = perlScript+"\n "+path1+" \n "+path2+" \n "+outPath;
                txtScriptTwoFC7.setText(path2);
                txtScriptTwoFC7.setBackground(Color.WHITE);
        	}
        });
        btnCsvTwoFC7.setBounds(536, 284, 121, 25);
        panelFC7.add(btnCsvTwoFC7);
        
        JLabel lblMetadataFile = new JLabel("MetaData File :");
        lblMetadataFile.setForeground(new Color(0, 128, 0));
        lblMetadataFile.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblMetadataFile.setBounds(50, 278, 158, 31);
        panelFC7.add(lblMetadataFile);
        
        JLabel label_5 = new JLabel("OutPath:");
        label_5.setForeground(new Color(0, 128, 0));
        label_5.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        label_5.setBounds(105, 320, 91, 31);
        panelFC7.add(label_5);
        
        JButton btnClearFC7 = new JButton("Clear Fields");
        btnClearFC7.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                txtScriptPerlFC7.setText(null);
                txtScriptPerlFC7.setBackground(Color.ORANGE);
                txtScriptOneFC7.setText(null);
                txtScriptOneFC7.setBackground(Color.ORANGE);
                txtScriptTwoFC7.setText(null);
                txtScriptTwoFC7.setBackground(Color.ORANGE);
                txtOutPathFC7.setText(null);
                txtOutPathFC7.setBackground(Color.ORANGE);
                receivedMessageFC7=null;
                cmbScriptOptionFC7.setSelectedIndex(0);
            }
        });
        btnClearFC7.setForeground(Color.WHITE);
        btnClearFC7.setBackground(new Color(0, 204, 204));
        btnClearFC7.setBounds(294, 496, 102, 23);
        panelFC7.add(btnClearFC7);
        
        JLabel lblFc = new JLabel("FC7");
        lblFc.setForeground(new Color(0, 128, 0));
        lblFc.setFont(new Font("Noto Sans CJK TC Regular", Font.BOLD | Font.ITALIC, 30));
        lblFc.setBounds(91, 12, 91, 82);
        panelFC7.add(lblFc);
        
        JPanel panel = new JPanel();
        panel.setLayout(null);
        panel.setBackground(Color.WHITE);
        tabbedPane.addTab("VH5 PROCESS", null, panel, null);
        
        JLabel label_4 = new JLabel("");
        label_4.setIcon(new ImageIcon(CabbageExecuterService.class.getResource("/com/cabbage/service/images/vamplogo.jpg")));
        label_4.setBounds(285, 25, 131, 130);
        panel.add(label_4);
        
        JLabel lblSelectAexe = new JLabel("VH5cmdl.exe  :");
        lblSelectAexe.setForeground(Color.BLUE);
        lblSelectAexe.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblSelectAexe.setBounds(54, 171, 154, 31);
        panel.add(lblSelectAexe);
        
        JButton btnVampExe = new JButton("VAMP EXE");
        btnVampExe.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                setScriptsPathsVH5VAMP("VAMP");
            }
        });
        btnVampExe.setBounds(523, 177, 121, 25);
        panel.add(btnVampExe);
        
        JComboBox cmbStrandVamp = new JComboBox();
        cmbStrandVamp.setModel(new DefaultComboBoxModel(new String[] {"direct", "comple", "both"}));
        cmbStrandVamp.setBounds(185, 395, 91, 31);
        panel.add(cmbStrandVamp);
        
        JButton btnExecuteExeVamp = new JButton("Execute Exe!");
        btnExecuteExeVamp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String receivedMessage;
                String exeVampFile;
                String probeFileVamp;
                String targetListVamp;
                String outFileVamp;
                String gcutOffVamp;
                String misMatchesVamp;
                String cmbStrand;

                
                    if(txtExeVampFile.getText().isEmpty() || txtProbeFileVamp.getText().isEmpty() || txtTargetListVamp.getText().isEmpty() || 
                            txtOutFileVamp.getText().isEmpty() || txtGcutOffVamp.getText().isEmpty() || txtMisMatchesVamp.getText().isEmpty()) {
                        JOptionPane.showMessageDialog(null,"NO TXT TO PROCESS, PLEASE SELECT TXT OR EXE FILE");
                    }else {
                        exeVampFile = txtExeVampFile.getText();
                        probeFileVamp =  txtProbeFileVamp.getText();
                        targetListVamp = txtTargetListVamp.getText();
                        outFileVamp = txtOutFileVamp.getText();
                        gcutOffVamp = txtGcutOffVamp.getText();
                        misMatchesVamp = txtMisMatchesVamp.getText();
                        cmbStrand = (String)cmbStrandVamp.getSelectedItem();
                        receivedMessage = JavaPerlService.ExePathVampVH5(exeVampFile,probeFileVamp,targetListVamp,outFileVamp,gcutOffVamp,misMatchesVamp,cmbStrand);
                        System.out.println("RECEIVED MESSAGE: "+receivedMessage);
                    }
            }
        });
        btnExecuteExeVamp.setBackground(Color.GREEN);
        btnExecuteExeVamp.setBounds(286, 460, 154, 31);
        panel.add(btnExecuteExeVamp);
        
        JLabel lblprobefile = new JLabel("-PROBEFILE :");
        lblprobefile.setForeground(Color.BLUE);
        lblprobefile.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblprobefile.setBounds(54, 209, 137, 31);
        panel.add(lblprobefile);
        
        JButton btnProbefileTxt = new JButton("PROBEFILE txt");
        btnProbefileTxt.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                String path = JavaPerlService.fileChooser();
                txtProbeFileVamp.setText(path);
                txtProbeFileVamp.setBackground(Color.WHITE);
            }
        });
        btnProbefileTxt.setBounds(523, 215, 121, 25);
        panel.add(btnProbefileTxt);
        
        JButton btnTargetlistTxt = new JButton("TARGETLIST txt");
        btnTargetlistTxt.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String path = JavaPerlService.fileChooser();                
                txtTargetListVamp.setText(path);
                txtTargetListVamp.setBackground(Color.WHITE);
            }
        });
        btnTargetlistTxt.setBounds(523, 257, 121, 25);
        panel.add(btnTargetlistTxt);
        
        JLabel lbltargetlist = new JLabel("-TARGETLIST :");
        lbltargetlist.setForeground(Color.BLUE);
        lbltargetlist.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lbltargetlist.setBounds(42, 251, 156, 31);
        panel.add(lbltargetlist);
        
        JLabel lblgcutoff = new JLabel("-GCUTOFF :");
        lblgcutoff.setForeground(Color.BLUE);
        lblgcutoff.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblgcutoff.setBounds(63, 335, 131, 31);
        panel.add(lblgcutoff);
        
        txtExeVampFile = new JTextField();
        txtExeVampFile.setToolTipText("Choose Perl Script");
        txtExeVampFile.setForeground(Color.BLACK);
        txtExeVampFile.setFont(new Font("Dialog", Font.BOLD, 14));
        txtExeVampFile.setEnabled(false);
        txtExeVampFile.setColumns(10);
        txtExeVampFile.setBackground(Color.ORANGE);
        txtExeVampFile.setBounds(218, 174, 295, 27);
        panel.add(txtExeVampFile);
        
        txtProbeFileVamp = new JTextField();
        txtProbeFileVamp.setToolTipText("Choose CSV 1");
        txtProbeFileVamp.setForeground(Color.BLACK);
        txtProbeFileVamp.setFont(new Font("Dialog", Font.BOLD, 14));
        txtProbeFileVamp.setEnabled(false);
        txtProbeFileVamp.setColumns(10);
        txtProbeFileVamp.setBackground(Color.ORANGE);
        txtProbeFileVamp.setBounds(217, 212, 296, 27);
        panel.add(txtProbeFileVamp);
        
        txtTargetListVamp = new JTextField();
        txtTargetListVamp.setToolTipText("Choose CSV 2");
        txtTargetListVamp.setForeground(Color.BLACK);
        txtTargetListVamp.setFont(new Font("Dialog", Font.BOLD, 14));
        txtTargetListVamp.setEnabled(false);
        txtTargetListVamp.setColumns(10);
        txtTargetListVamp.setBackground(Color.ORANGE);
        txtTargetListVamp.setBounds(218, 254, 295, 27);
        panel.add(txtTargetListVamp);
        
        txtGcutOffVamp = new JTextField();
        txtGcutOffVamp.setText("0");
        txtGcutOffVamp.setToolTipText("");
        txtGcutOffVamp.setForeground(Color.BLACK);
        txtGcutOffVamp.setFont(new Font("Dialog", Font.BOLD, 14));
        txtGcutOffVamp.setColumns(10);
        txtGcutOffVamp.setBackground(Color.WHITE);
        txtGcutOffVamp.setBounds(204, 338, 33, 27);
        panel.add(txtGcutOffVamp);
        
        JButton button_4 = new JButton("Clear Fields");
        button_4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                txtExeVampFile.setText(null);
                txtExeVampFile.setBackground(Color.ORANGE);
                txtProbeFileVamp.setText(null);
                txtProbeFileVamp.setBackground(Color.ORANGE);
                txtTargetListVamp.setText(null);
                txtTargetListVamp.setBackground(Color.ORANGE);
                txtOutFileVamp.setText(null);
                txtGcutOffVamp.setText("0");
                txtMisMatchesVamp.setText("0");
                cmbStrandVamp.setSelectedIndex(0);
            }
        });
        button_4.setForeground(Color.WHITE);
        button_4.setBackground(new Color(0, 204, 204));
        button_4.setBounds(314, 521, 102, 23);
        panel.add(button_4);
        
        txtOutFileVamp = new JTextField();
        txtOutFileVamp.setText("vh5fileOut");
        txtOutFileVamp.setToolTipText("");
        txtOutFileVamp.setForeground(Color.BLACK);
        txtOutFileVamp.setFont(new Font("Dialog", Font.BOLD, 14));
        txtOutFileVamp.setColumns(10);
        txtOutFileVamp.setBackground(Color.WHITE);
        txtOutFileVamp.setBounds(204, 296, 115, 27);
        panel.add(txtOutFileVamp);
        
        JLabel lbloutfile = new JLabel("-OUTFILE :");
        lbloutfile.setForeground(Color.BLUE);
        lbloutfile.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lbloutfile.setBounds(77, 293, 121, 31);
        panel.add(lbloutfile);
        
        JLabel lblVamp = new JLabel("VH5");
        lblVamp.setForeground(Color.BLUE);
        lblVamp.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 30));
        lblVamp.setBounds(87, 25, 91, 82);
        panel.add(lblVamp);
        
        JLabel lblmismatches = new JLabel("-MISMATCHES :");
        lblmismatches.setForeground(Color.BLUE);
        lblmismatches.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblmismatches.setBounds(285, 335, 163, 31);
        panel.add(lblmismatches);
        
        txtMisMatchesVamp = new JTextField();
        txtMisMatchesVamp.setText("0");
        txtMisMatchesVamp.setToolTipText("");
        txtMisMatchesVamp.setForeground(Color.BLACK);
        txtMisMatchesVamp.setFont(new Font("Dialog", Font.BOLD, 14));
        txtMisMatchesVamp.setColumns(10);
        txtMisMatchesVamp.setBackground(Color.WHITE);
        txtMisMatchesVamp.setBounds(458, 338, 33, 27);
        panel.add(txtMisMatchesVamp);
        
        JLabel lblstrand = new JLabel("-STRAND :");
        lblstrand.setForeground(Color.BLUE);
        lblstrand.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblstrand.setBounds(63, 395, 115, 31);
        panel.add(lblstrand);
        
        JLabel lbltxt = new JLabel(".txt");
        lbltxt.setForeground(Color.BLACK);
        lbltxt.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lbltxt.setBounds(319, 293, 41, 31);
        panel.add(lbltxt);
        
        JPanel panel_1 = new JPanel();
        tabbedPane.addTab("VFAT PROCESS", null, panel_1, null);
        panel_1.setLayout(null);
        panel_1.setBackground(Color.WHITE);
        
        JLabel label = new JLabel("");
        label.setIcon(new ImageIcon(CabbageExecuterService.class.getResource("/com/cabbage/service/images/vamplogo.jpg")));
        label.setBounds(285, 25, 131, 130);
        panel_1.add(label);
        
        JLabel lblVfatexe = new JLabel("VFAT.exe :");
        lblVfatexe.setForeground(Color.BLUE);
        lblVfatexe.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblVfatexe.setBounds(93, 171, 115, 31);
        panel_1.add(lblVfatexe);
        
        JButton button = new JButton("VAMP EXE");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                setScriptsPathsVFATVAMP("VAMP");  
            }
        });
        button.setBounds(523, 177, 121, 25);
        panel_1.add(button);
        
        JComboBox cmbVfatTrackext = new JComboBox();
        cmbVfatTrackext.setModel(new DefaultComboBoxModel(new String[] {"NO", "YES"}));
        cmbVfatTrackext.setBounds(185, 467, 91, 31);
        panel_1.add(cmbVfatTrackext);
        
        JComboBox cmbVfatFormat = new JComboBox();
        cmbVfatFormat.setModel(new DefaultComboBoxModel(new String[] {"MEGA", "PHYLIP", "NEXUS"}));
        cmbVfatFormat.setBounds(553, 334, 115, 31);
        panel_1.add(cmbVfatFormat);
        
        JComboBox cmbVfatMode = new JComboBox();
        cmbVfatMode.setModel(new DefaultComboBoxModel(new String[] {"CHAR", "DISTANCE", "BOOTSTRAP"}));
        cmbVfatMode.setBounds(130, 377, 115, 31);
        panel_1.add(cmbVfatMode);
        
        JComboBox cmbVfatModel = new JComboBox();
        cmbVfatModel.setModel(new DefaultComboBoxModel(new String[] {"NEI", "BROWN"}));
        cmbVfatModel.setBounds(350, 377, 66, 31);
        panel_1.add(cmbVfatModel);
        
        JComboBox cmbVfatCount = new JComboBox();
        cmbVfatCount.setModel(new DefaultComboBoxModel(new String[] {"DEGENERATED", "SINGLE"}));
        cmbVfatCount.setBounds(553, 380, 115, 31);
        panel_1.add(cmbVfatCount);
        
        JComboBox cmbVfatTracking = new JComboBox();
        cmbVfatTracking.setModel(new DefaultComboBoxModel(new String[] {"NO", "YES"}));
        cmbVfatTracking.setBounds(185, 425, 91, 31);
        panel_1.add(cmbVfatTracking);
        
        JButton btnExecuteVfat = new JButton("Execute Exe!");
        btnExecuteVfat.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                String receivedMessage;
                String vfatExeVamp;
                String vFatVhFile;
                String targetListVfat;
                String vfatOutFile;
                String vfatLeftText;
                String vfatRightText;
                String vfatThreshold;
                String vfatFormat;
                String vfatMode;
                String vfatModel;
                String vfatCount;
                String vfatTracking;
                String vfatTrackFile;
                String vfatTrackext;
                String vfatReplicates;

                    if(txtVfatExeVamp.getText().isEmpty() || txtVFatVhFile.getText().isEmpty() || txtTargetListVfat.getText().isEmpty() || 
                            txtVfatOutFile.getText().isEmpty() || txtVfatLeftText.getText().isEmpty() || txtVfatRightText.getText().isEmpty() 
                            || txtVfatThreshold.getText().isEmpty() || txtVfatTrackFile.getText().isEmpty() || txtVfatReplicates.getText().isEmpty()) {
                        JOptionPane.showMessageDialog(null,"NO TXT TO PROCESS, PLEASE SELECT TXT OR EXE FILE");
                    }else {
                        vfatExeVamp = txtVfatExeVamp.getText();
                        vFatVhFile =  txtVFatVhFile.getText();
                        targetListVfat = txtTargetListVfat.getText();
                        vfatOutFile = txtVfatOutFile.getText();
                        vfatLeftText = txtVfatLeftText.getText();
                        vfatRightText = txtVfatRightText.getText();
                        vfatThreshold = txtVfatThreshold.getText();
                        vfatTrackFile = txtVfatTrackFile.getText();
                        vfatReplicates = txtVfatReplicates.getText();

                      vfatFormat = (String) cmbVfatFormat.getSelectedItem();
                      vfatMode = (String) cmbVfatMode.getSelectedItem();
                      vfatModel = (String) cmbVfatModel.getSelectedItem();
                      vfatCount = (String) cmbVfatCount.getSelectedItem();
                      vfatTracking = (String) cmbVfatTracking.getSelectedItem();
                      vfatTrackext = (String) cmbVfatTrackext.getSelectedItem();
                        
                        receivedMessage = JavaPerlService.ExePathVampVFAT(vfatExeVamp,vFatVhFile,targetListVfat,vfatOutFile,vfatLeftText,vfatRightText,vfatThreshold,
                                vfatFormat,vfatMode,vfatModel,vfatCount,vfatTracking,vfatTrackFile,vfatTrackext,vfatReplicates);
                        System.out.println("RECEIVED MESSAGE: "+receivedMessage);
                    }
            }
        });
        btnExecuteVfat.setBackground(Color.GREEN);
        btnExecuteVfat.setBounds(273, 531, 154, 31);
        panel_1.add(btnExecuteVfat);
        
        JLabel lblvhfile = new JLabel("-VHFILE :");
        lblvhfile.setForeground(Color.BLUE);
        lblvhfile.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblvhfile.setBounds(93, 213, 102, 31);
        panel_1.add(lblvhfile);
        
        JButton btnVhfileTxt = new JButton("VHFILE txt");
        btnVhfileTxt.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String path = JavaPerlService.fileChooser();
                txtVFatVhFile.setText(path);
                txtVFatVhFile.setBackground(Color.WHITE);
            }
        });
        btnVhfileTxt.setBounds(523, 215, 121, 25);
        panel_1.add(btnVhfileTxt);
        
        JButton button_3 = new JButton("TARGETLIST txt");
        button_3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String path = JavaPerlService.fileChooser();
                txtTargetListVfat.setText(path);
                txtTargetListVfat.setBackground(Color.WHITE);
            }
        });
        button_3.setBounds(523, 257, 121, 25);
        panel_1.add(button_3);
        
        JLabel label_9 = new JLabel("-TARGETLIST :");
        label_9.setForeground(Color.BLUE);
        label_9.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        label_9.setBounds(42, 251, 156, 31);
        panel_1.add(label_9);
        
        JLabel lblcounts = new JLabel("-COUNTS :");
        lblcounts.setForeground(Color.BLUE);
        lblcounts.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblcounts.setBounds(435, 377, 115, 31);
        panel_1.add(lblcounts);
        
        txtVfatExeVamp = new JTextField();
        txtVfatExeVamp.setToolTipText("Choose Perl Script");
        txtVfatExeVamp.setForeground(Color.BLACK);
        txtVfatExeVamp.setFont(new Font("Dialog", Font.BOLD, 14));
        txtVfatExeVamp.setEnabled(false);
        txtVfatExeVamp.setColumns(10);
        txtVfatExeVamp.setBackground(Color.ORANGE);
        txtVfatExeVamp.setBounds(218, 174, 295, 27);
        panel_1.add(txtVfatExeVamp);
        
        txtVFatVhFile = new JTextField();
        txtVFatVhFile.setToolTipText("Choose CSV 1");
        txtVFatVhFile.setForeground(Color.BLACK);
        txtVFatVhFile.setFont(new Font("Dialog", Font.BOLD, 14));
        txtVFatVhFile.setEnabled(false);
        txtVFatVhFile.setColumns(10);
        txtVFatVhFile.setBackground(Color.ORANGE);
        txtVFatVhFile.setBounds(217, 212, 296, 27);
        panel_1.add(txtVFatVhFile);
        
        txtTargetListVfat = new JTextField();
        txtTargetListVfat.setToolTipText("Choose CSV 2");
        txtTargetListVfat.setForeground(Color.BLACK);
        txtTargetListVfat.setFont(new Font("Dialog", Font.BOLD, 14));
        txtTargetListVfat.setEnabled(false);
        txtTargetListVfat.setColumns(10);
        txtTargetListVfat.setBackground(Color.ORANGE);
        txtTargetListVfat.setBounds(218, 254, 295, 27);
        panel_1.add(txtTargetListVfat);
        
        txtVfatRightText = new JTextField();
        txtVfatRightText.setToolTipText("");
        txtVfatRightText.setText("0");
        txtVfatRightText.setForeground(Color.BLACK);
        txtVfatRightText.setFont(new Font("Dialog", Font.BOLD, 14));
        txtVfatRightText.setColumns(10);
        txtVfatRightText.setBackground(Color.WHITE);
        txtVfatRightText.setBounds(185, 338, 33, 27);
        panel_1.add(txtVfatRightText);
        
        JButton btnVfatCleat = new JButton("Clear Fields");
        btnVfatCleat.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                txtVfatExeVamp.setText(null);
                txtVfatExeVamp.setBackground(Color.ORANGE);
                txtVFatVhFile.setText(null);
                txtVFatVhFile.setBackground(Color.ORANGE);
                txtTargetListVfat.setText(null);
                txtTargetListVfat.setBackground(Color.ORANGE);
                txtVfatOutFile.setText("vfatOutFile");
                txtVfatLeftText.setText("0");
                txtVfatRightText.setText("0");
                txtVfatThreshold.setText("-1");
                txtVfatTrackFile.setText("track");
                txtVfatReplicates.setText("1");
                cmbVfatFormat.setSelectedIndex(0);
                cmbVfatMode.setSelectedIndex(0);
                cmbVfatModel.setSelectedIndex(0);
                cmbVfatCount.setSelectedIndex(0);
                cmbVfatTracking.setSelectedIndex(0);
                cmbVfatTrackext.setSelectedIndex(0);
            }
        });
        btnVfatCleat.setForeground(Color.WHITE);
        btnVfatCleat.setBackground(new Color(0, 204, 204));
        btnVfatCleat.setBounds(304, 573, 102, 23);
        panel_1.add(btnVfatCleat);
        
        txtVfatOutFile = new JTextField();
        txtVfatOutFile.setToolTipText("");
        txtVfatOutFile.setText("vfatOutFile");
        txtVfatOutFile.setForeground(Color.BLACK);
        txtVfatOutFile.setFont(new Font("Dialog", Font.BOLD, 14));
        txtVfatOutFile.setColumns(10);
        txtVfatOutFile.setBackground(Color.WHITE);
        txtVfatOutFile.setBounds(156, 296, 115, 27);
        panel_1.add(txtVfatOutFile);
        
        JLabel label_12 = new JLabel("-OUTFILE :");
        label_12.setForeground(Color.BLUE);
        label_12.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        label_12.setBounds(42, 293, 121, 31);
        panel_1.add(label_12);
        
        JLabel lblVfat = new JLabel("VFAT");
        lblVfat.setForeground(Color.BLUE);
        lblVfat.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 30));
        lblVfat.setBounds(87, 25, 91, 82);
        panel_1.add(lblVfat);
        
        JLabel lbltracking = new JLabel("-TRACKING :");
        lbltracking.setForeground(Color.BLUE);
        lbltracking.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lbltracking.setBounds(42, 425, 163, 31);
        panel_1.add(lbltracking);
        
        txtVfatLeftText = new JTextField();
        txtVfatLeftText.setToolTipText("");
        txtVfatLeftText.setText("0");
        txtVfatLeftText.setForeground(Color.BLACK);
        txtVfatLeftText.setFont(new Font("Dialog", Font.BOLD, 14));
        txtVfatLeftText.setColumns(10);
        txtVfatLeftText.setBackground(Color.WHITE);
        txtVfatLeftText.setBounds(461, 296, 33, 27);
        panel_1.add(txtVfatLeftText);
        
        JLabel lbltrackext = new JLabel("-TRACKEXT :");
        lbltrackext.setForeground(Color.BLUE);
        lbltrackext.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lbltrackext.setBounds(42, 467, 136, 31);
        panel_1.add(lbltrackext);
        
        JLabel label_16 = new JLabel(".txt");
        label_16.setForeground(Color.BLACK);
        label_16.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        label_16.setBounds(273, 293, 41, 31);
        panel_1.add(label_16);
        
        JLabel lbllefttext = new JLabel("-LEFTTEXT :");
        lbllefttext.setForeground(Color.BLUE);
        lbllefttext.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lbllefttext.setBounds(324, 293, 131, 31);
        panel_1.add(lbllefttext);
        
        JLabel lblrightext = new JLabel("-RIGHTEXT :");
        lblrightext.setForeground(Color.BLUE);
        lblrightext.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblrightext.setBounds(42, 335, 131, 31);
        panel_1.add(lblrightext);
        
        JLabel lblthreshold = new JLabel("-THRESHOLD :");
        lblthreshold.setForeground(Color.BLUE);
        lblthreshold.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblthreshold.setBounds(228, 335, 154, 31);
        panel_1.add(lblthreshold);
        
        JLabel lblformat = new JLabel("-FORMAT :");
        lblformat.setForeground(Color.BLUE);
        lblformat.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblformat.setBounds(435, 335, 115, 31);
        panel_1.add(lblformat);
        
        JLabel lblmode = new JLabel("-MODE :");
        lblmode.setForeground(Color.BLUE);
        lblmode.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblmode.setBounds(42, 377, 91, 31);
        panel_1.add(lblmode);
        
        JLabel lblmodel = new JLabel("-MODEL :");
        lblmodel.setForeground(Color.BLUE);
        lblmodel.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblmodel.setBounds(251, 377, 102, 31);
        panel_1.add(lblmodel);
        
        JLabel lbltrackfile = new JLabel("-TRACKFILE :");
        lbltrackfile.setForeground(Color.BLUE);
        lbltrackfile.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lbltrackfile.setBounds(310, 425, 145, 31);
        panel_1.add(lbltrackfile);
        
        txtVfatThreshold = new JTextField();
        txtVfatThreshold.setToolTipText("");
        txtVfatThreshold.setText("-1");
        txtVfatThreshold.setForeground(Color.BLACK);
        txtVfatThreshold.setFont(new Font("Dialog", Font.BOLD, 14));
        txtVfatThreshold.setColumns(10);
        txtVfatThreshold.setBackground(Color.WHITE);
        txtVfatThreshold.setBounds(392, 338, 33, 27);
        panel_1.add(txtVfatThreshold);
        
        txtVfatTrackFile = new JTextField();
        txtVfatTrackFile.setToolTipText("");
        txtVfatTrackFile.setText("track");
        txtVfatTrackFile.setForeground(Color.BLACK);
        txtVfatTrackFile.setFont(new Font("Dialog", Font.BOLD, 14));
        txtVfatTrackFile.setColumns(10);
        txtVfatTrackFile.setBackground(Color.WHITE);
        txtVfatTrackFile.setBounds(461, 429, 62, 27);
        panel_1.add(txtVfatTrackFile);
        
        JLabel label_3 = new JLabel(".txt");
        label_3.setForeground(Color.BLACK);
        label_3.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        label_3.setBounds(523, 425, 41, 31);
        panel_1.add(label_3);
        
        JLabel lblreplicates = new JLabel("-REPLICATES :");
        lblreplicates.setForeground(Color.BLUE);
        lblreplicates.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblreplicates.setBounds(304, 464, 149, 31);
        panel_1.add(lblreplicates);
        
        txtVfatReplicates = new JTextField();
        txtVfatReplicates.setToolTipText("");
        txtVfatReplicates.setText("1");
        txtVfatReplicates.setForeground(Color.BLACK);
        txtVfatReplicates.setFont(new Font("Dialog", Font.BOLD, 14));
        txtVfatReplicates.setColumns(10);
        txtVfatReplicates.setBackground(Color.WHITE);
        txtVfatReplicates.setBounds(461, 467, 33, 27);
        panel_1.add(txtVfatReplicates);
        
        JPanel panel_2 = new JPanel();
        panel_2.setLayout(null);
        panel_2.setBackground(Color.WHITE);
        tabbedPane.addTab("VHRP PROCESS", null, panel_2, null);
        
        JLabel label_6 = new JLabel("");
        label_6.setIcon(new ImageIcon(CabbageExecuterService.class.getResource("/com/cabbage/service/images/vamplogo.jpg")));
        label_6.setBounds(285, 25, 131, 130);
        panel_2.add(label_6);
        
        JLabel lblVhrpexe = new JLabel("VHRP.exe  :");
        lblVhrpexe.setForeground(Color.BLUE);
        lblVhrpexe.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblVhrpexe.setBounds(87, 171, 121, 31);
        panel_2.add(lblVhrpexe);
        
        JButton btnVhrpExe = new JButton("VHRP EXE");
        btnVhrpExe.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setScriptsPathsVHRPVAMP("VAMP");
            }
        });
        btnVhrpExe.setBounds(523, 177, 121, 25);
        panel_2.add(btnVhrpExe);
        
        JComboBox cmbSitesGlobalVHRP = new JComboBox();
        cmbSitesGlobalVHRP.setModel(new DefaultComboBoxModel(new String[] {"match", "sites", "bases"}));
        cmbSitesGlobalVHRP.setBounds(258, 372, 91, 31);
        panel_2.add(cmbSitesGlobalVHRP);
        
        JButton btnExecuteVHRP = new JButton("Execute Exe!");
        btnExecuteVHRP.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String receivedMessage;
                String vhrpExeVamp;
                String vhDataFileVHRP;
                String probeFileVHRP;
                String targetListVHRP;
                String globalFileVHRP;
                String sitesGlobalVHRP;

                
                    if(txtVhrpExeVamp.getText().isEmpty() || txtVhDataFileVHRP.getText().isEmpty() || txtProbeFileVHRP.getText().isEmpty() || 
                            txtTargetListVHRP.getText().isEmpty() || txtGlobalFileVHRP.getText().isEmpty()) {
                        JOptionPane.showMessageDialog(null,"NO TXT TO PROCESS, PLEASE SELECT TXT OR EXE FILE");
                    }else {
                        vhrpExeVamp = txtVhrpExeVamp.getText();
                        vhDataFileVHRP =  txtVhDataFileVHRP.getText();
                        probeFileVHRP = txtProbeFileVHRP.getText();
                        targetListVHRP = txtTargetListVHRP.getText();
                        globalFileVHRP = txtGlobalFileVHRP.getText();
                        sitesGlobalVHRP = (String)cmbSitesGlobalVHRP.getSelectedItem();
                        receivedMessage = JavaPerlService.ExePathVampVHRP(vhrpExeVamp,vhDataFileVHRP,probeFileVHRP,targetListVHRP,globalFileVHRP,sitesGlobalVHRP);
                        System.out.println("RECEIVED MESSAGE: "+receivedMessage);
                    }
            }
        });
        btnExecuteVHRP.setBackground(Color.GREEN);
        btnExecuteVHRP.setBounds(285, 452, 154, 31);
        panel_2.add(btnExecuteVHRP);
        
        JLabel lblvhdatafile = new JLabel("-VHDATAFILE:");
        lblvhdatafile.setForeground(Color.BLUE);
        lblvhdatafile.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblvhdatafile.setBounds(42, 209, 154, 31);
        panel_2.add(lblvhdatafile);
        
        JButton btnVhdatafileTxt = new JButton("VHDATAFILE txt");
        btnVhdatafileTxt.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String path = JavaPerlService.fileChooser();
                txtVhDataFileVHRP.setText(path);
                txtVhDataFileVHRP.setBackground(Color.WHITE);
            }
        });
        btnVhdatafileTxt.setBounds(523, 215, 121, 25);
        panel_2.add(btnVhdatafileTxt);
        
        JButton button_9 = new JButton("TARGETLIST txt");
        button_9.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String path = JavaPerlService.fileChooser();
                txtTargetListVHRP.setText(path);
                txtTargetListVHRP.setBackground(Color.WHITE);
            }
        });
        button_9.setBounds(523, 291, 121, 25);
        panel_2.add(button_9);
        
        JLabel label_14 = new JLabel("-TARGETLIST :");
        label_14.setForeground(Color.BLUE);
        label_14.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        label_14.setBounds(42, 285, 156, 31);
        panel_2.add(label_14);
        
        txtVhrpExeVamp = new JTextField();
        txtVhrpExeVamp.setToolTipText("Choose Perl Script");
        txtVhrpExeVamp.setForeground(Color.BLACK);
        txtVhrpExeVamp.setFont(new Font("Dialog", Font.BOLD, 14));
        txtVhrpExeVamp.setEnabled(false);
        txtVhrpExeVamp.setColumns(10);
        txtVhrpExeVamp.setBackground(Color.ORANGE);
        txtVhrpExeVamp.setBounds(218, 174, 295, 27);
        panel_2.add(txtVhrpExeVamp);
        
        txtVhDataFileVHRP = new JTextField();
        txtVhDataFileVHRP.setToolTipText("Choose CSV 1");
        txtVhDataFileVHRP.setForeground(Color.BLACK);
        txtVhDataFileVHRP.setFont(new Font("Dialog", Font.BOLD, 14));
        txtVhDataFileVHRP.setEnabled(false);
        txtVhDataFileVHRP.setColumns(10);
        txtVhDataFileVHRP.setBackground(Color.ORANGE);
        txtVhDataFileVHRP.setBounds(217, 212, 296, 27);
        panel_2.add(txtVhDataFileVHRP);
        
        txtTargetListVHRP = new JTextField();
        txtTargetListVHRP.setToolTipText("Choose CSV 2");
        txtTargetListVHRP.setForeground(Color.BLACK);
        txtTargetListVHRP.setFont(new Font("Dialog", Font.BOLD, 14));
        txtTargetListVHRP.setEnabled(false);
        txtTargetListVHRP.setColumns(10);
        txtTargetListVHRP.setBackground(Color.ORANGE);
        txtTargetListVHRP.setBounds(218, 288, 295, 27);
        panel_2.add(txtTargetListVHRP);
        
        JButton btnClearVHRP = new JButton("Clear Fields");
        btnClearVHRP.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                txtVhrpExeVamp.setText(null);
                txtVhrpExeVamp.setBackground(Color.ORANGE);
                txtVhDataFileVHRP.setText(null);
                txtVhDataFileVHRP.setBackground(Color.ORANGE);
                txtProbeFileVHRP.setText(null);
                txtProbeFileVHRP.setBackground(Color.ORANGE);
                txtTargetListVHRP.setText(null);
                txtTargetListVHRP.setBackground(Color.ORANGE);
                txtGlobalFileVHRP.setText("globalFileOut");
                cmbSitesGlobalVHRP.setSelectedIndex(0);
            }
        });
        btnClearVHRP.setForeground(Color.WHITE);
        btnClearVHRP.setBackground(new Color(0, 204, 204));
        btnClearVHRP.setBounds(314, 513, 102, 23);
        panel_2.add(btnClearVHRP);
        
        txtGlobalFileVHRP = new JTextField();
        txtGlobalFileVHRP.setToolTipText("");
        txtGlobalFileVHRP.setText("globalFileOut");
        txtGlobalFileVHRP.setForeground(Color.BLACK);
        txtGlobalFileVHRP.setFont(new Font("Dialog", Font.BOLD, 14));
        txtGlobalFileVHRP.setColumns(10);
        txtGlobalFileVHRP.setBackground(Color.WHITE);
        txtGlobalFileVHRP.setBounds(228, 326, 115, 27);
        panel_2.add(txtGlobalFileVHRP);
        
        JLabel lblglobalfile = new JLabel("-GLOBALFILE :");
        lblglobalfile.setForeground(Color.BLUE);
        lblglobalfile.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblglobalfile.setBounds(74, 327, 154, 31);
        panel_2.add(lblglobalfile);
        
        JLabel lblVhrp = new JLabel("VHRP");
        lblVhrp.setForeground(Color.BLUE);
        lblVhrp.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 30));
        lblVhrp.setBounds(87, 25, 91, 82);
        panel_2.add(lblVhrp);
        
        JLabel lblsitesglobal = new JLabel("-SITESGLOBAL :");
        lblsitesglobal.setForeground(Color.BLUE);
        lblsitesglobal.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblsitesglobal.setBounds(81, 369, 167, 31);
        panel_2.add(lblsitesglobal);
        
        JLabel label_21 = new JLabel(".txt");
        label_21.setForeground(Color.BLACK);
        label_21.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        label_21.setBounds(342, 323, 41, 31);
        panel_2.add(label_21);
        
        JLabel lblprobefile_1 = new JLabel("-PROBEFILE :");
        lblprobefile_1.setForeground(Color.BLUE);
        lblprobefile_1.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 19));
        lblprobefile_1.setBounds(52, 251, 138, 31);
        panel_2.add(lblprobefile_1);
        
        txtProbeFileVHRP = new JTextField();
        txtProbeFileVHRP.setToolTipText("Choose CSV 1");
        txtProbeFileVHRP.setForeground(Color.BLACK);
        txtProbeFileVHRP.setFont(new Font("Dialog", Font.BOLD, 14));
        txtProbeFileVHRP.setEnabled(false);
        txtProbeFileVHRP.setColumns(10);
        txtProbeFileVHRP.setBackground(Color.ORANGE);
        txtProbeFileVHRP.setBounds(217, 250, 296, 27);
        panel_2.add(txtProbeFileVHRP);
        
        JButton button_11 = new JButton("PROBEFILE txt");
        button_11.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String path = JavaPerlService.fileChooser();
                txtProbeFileVHRP.setText(path);
                txtProbeFileVHRP.setBackground(Color.WHITE);
            }
        });
        button_11.setBounds(523, 251, 121, 25);
        panel_2.add(button_11);
        
    }
    
    public void setScriptsPathsFC7(String type) {
    	String path = JavaPerlService.fileChooser();
        String outpath = PathComplementUtil.getOutPath(path);
        String metaFile = null;
        String trainingFile = null;
        boolean validPath = PathComplementUtil.getOutPathCount(outpath);
        if(validPath) {
            trainingFile = PathComplementUtil.getCsvFileTraining(outpath,type);
            metaFile = PathComplementUtil.getCsvFileMetadata(outpath,type);                 
            
            txtScriptOneFC7.setText(trainingFile);
            txtScriptOneFC7.setBackground(Color.WHITE);
            
            txtScriptTwoFC7.setText(metaFile);
            txtScriptTwoFC7.setBackground(Color.WHITE);
            
            txtScriptPerlFC7.setText(path);
            txtScriptPerlFC7.setBackground(Color.WHITE);
            
            txtOutPathFC7.setText(outpath);
            txtOutPathFC7.setBackground(Color.WHITE);
            
//            txtReceivedMessageFC7.setText(null);
//            txtReceivedMessageFC7.setText(path+" \n "+trainingFile+" \n "+metaFile+" \n "+outpath+" \n ");
            receivedMessageFC7 = null;
            receivedMessageFC7 = path+" \n "+trainingFile+" \n "+metaFile+" \n "+outpath+" \n ";
            
        }else {
//            txtReceivedMessageFC7.setText(null);
//            txtReceivedMessageFC7.setText("The PerlScript shoud be inside --> CAB5.0\\src\\bin\\ \n to find the csv files");
            JOptionPane.showMessageDialog(null,"The PerlScript May Not Work! See Details At Script Execution Message Section Please!");
            JOptionPane.showMessageDialog(null,"The DEFAULT OUTPATH will be "+outpath);
            txtScriptPerlFC7.setText(path);
            txtScriptPerlFC7.setBackground(Color.WHITE);
            txtOutPathFC7.setText(outpath);
            txtOutPathFC7.setBackground(Color.WHITE);
        }

    }
    
    public void setScriptsPathsBC4(String type) {
    	String path = JavaPerlService.fileChooser();
        String outpath = PathComplementUtil.getOutPath(path);
        String metaFile = null;
        String trainFile = null;
        String queryFile = null;
        boolean validPath = PathComplementUtil.getOutPathCount(outpath);
        
        if(validPath) {
        	trainFile = PathComplementUtil.getCsvFileTraining(outpath,type);
            metaFile = PathComplementUtil.getCsvFileMetadata(outpath,type);                 
            queryFile = PathComplementUtil.getCsvFileQuery(outpath);
            
            txtScriptOneBC4.setText(trainFile);
            txtScriptOneBC4.setBackground(Color.WHITE);
            
            txtScriptTwoBC4.setText(metaFile);
            txtScriptTwoBC4.setBackground(Color.WHITE);
            
            txtScriptThreeBC4.setText(queryFile);
            txtScriptThreeBC4.setBackground(Color.WHITE);
            
            txtScriptPerlBC4.setText(path);
            txtScriptPerlBC4.setBackground(Color.WHITE);
            
            txtOutPathBC4.setText(outpath);
            txtOutPathBC4.setBackground(Color.WHITE);
            
//            txtReceivedMessageBC4.setText(null);
//            txtReceivedMessageBC4.setText(path+" \n "+trainFile+" \n "+metaFile+" \n "+queryFile+" \n "+outpath+" \n ");
            receivedMessageBC4 = null;
            receivedMessageBC4 = path+" \n "+trainFile+" \n "+metaFile+" \n "+queryFile+" \n "+outpath+" \n ";
            
        }else {
//        	txtReceivedMessageBC4.setText(null);
//        	txtReceivedMessageBC4.setText("The PerlScript shoud be inside --> CAB5.0\\src\\bin\\ \n to find the csv files");
            JOptionPane.showMessageDialog(null,"The PerlScript May Not Work! See Details At Script Execution Message Section Please!");
            JOptionPane.showMessageDialog(null,"The DEFAULT OUTPATH will be "+outpath);
            txtScriptPerlBC4.setText(path);
            txtScriptPerlBC4.setBackground(Color.WHITE);
            txtOutPathBC4.setText(outpath);
            txtOutPathBC4.setBackground(Color.WHITE);
        }

    }
    
    public void setScriptsPathsVH5VAMP(String type) {
        String path = JavaPerlService.fileChooser();
            txtExeVampFile.setText(path);
            txtExeVampFile.setBackground(Color.WHITE);   
    }
    
    public void setScriptsPathsVFATVAMP(String type) {
        String path = JavaPerlService.fileChooser();
        txtVfatExeVamp.setText(path);
        txtVfatExeVamp.setBackground(Color.WHITE);   
    }
    
    public void setScriptsPathsVHRPVAMP(String type) {
        String path = JavaPerlService.fileChooser();
        txtVhrpExeVamp.setText(path);
        txtVhrpExeVamp.setBackground(Color.WHITE);   
    }
}
