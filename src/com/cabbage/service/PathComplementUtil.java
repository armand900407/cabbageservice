/*
 *      File: PathComplementUtil.java
 *    Author: Armando Perea <APerea@express-scripts.com>
 *      Date: Apr 04, 2019
 * Copyright: Express Scripts 2019
 */

/**
 * 
 */
package com.cabbage.service;


/**
 * @author Armando Perea &lt;APerea@express-scripts.com&gt;
 * @version 1.0.0
 * @since 1.0.0
 */
public class PathComplementUtil {
    
    public static final String METADATA_PATH_FC7="DatosParaFC7\\metadatafile.csv";
    
    public static final String TRAINING_PATH_FC7="DatosParaFC7\\trainingfile.csv";
    
    public static final String META_BC4="DatosParaBC4\\500meta.csv";
    
    public static final String QUERY_BC4="DatosParaBC4\\500query.csv";
    
    public static final String TRAIN_BC4="DatosParaBC4\\500train.csv";
    
    public static void main(String[] args) {
        //String path = " C:\\Users\\ArmandoPereaSanchez\\Documents\\Perl\\CAB5.0\\src\\bin\\FC7.pl";
        //String path = " C:\\Users\\FC7.pl";
        //String path ="C:\\CAB5.0\\src\\bin\\BC4.pl";
        String path ="C:\\CAB5.0\\src\\bin\\BC4.pl";
        String outPath;
        String meta=null;
        String training=null;
        String query=null;
        outPath = PathComplementUtil.getOutPath(path);
        if(PathComplementUtil.getOutPathCount(outPath)) {
        meta = PathComplementUtil.getCsvFileTraining(outPath,"BC4");
        training = PathComplementUtil.getCsvFileMetadata(outPath,"BC4");
        query = PathComplementUtil.getCsvFileQuery(outPath);
        }
        System.out.println("OUTPATH: "+outPath);
        System.out.println("METADATA: "+meta);
        System.out.println("TRAINING: "+training);
        System.out.println("QUERY: "+query);
    }
    
    
    public static String getOutPath(String path) {
        String[] parts = path.split("\\\\");
        String newPath= "";
        int count = 1;
        int pathIndex;
        pathIndex = parts.length-2;
        if(pathIndex>=3) {
            for(String pa : parts) {
                if(count < pathIndex) {
                    newPath = newPath+pa+"\\";
                    count++;
                }     
            }
            }else {
                newPath = newPath+"C:\\";
            }
        return newPath;
    }
    
    public static boolean getOutPathCount(String path) {
        String[] parts = path.split("\\\\");
        boolean flag = false;
        int pathIndex;
        pathIndex = parts.length-2;
        if(pathIndex>=0) {
            flag=true;
            }
        return flag;
    }
    
    public static String getCsvFileTraining(String path,String type) {
        String newPath;
        if(type.equals("FC7")) {
        newPath = path+TRAINING_PATH_FC7;
        }else {
        newPath = path+TRAIN_BC4;	
        }
        return newPath;
    }
    
    public static String getCsvFileMetadata(String path,String type) {
        String newPath;
        if(type.equals("FC7")) {
            newPath = path+METADATA_PATH_FC7;
            }else {
            newPath = path+META_BC4;	
            }
        return newPath;
    }
    
    public static String getCsvFileQuery(String path) {
        String newPath;
        newPath = path+QUERY_BC4;	
        return newPath;
    }
    
   


}
